var express = require('express');
var router = express.Router();
var elastic = require('../elasticsearch');
var Multer = require('multer');
const format = require('util').format;
const nodemailer = require('nodemailer');

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const superSecret = "sjfsdfs5646";
const payload = {
    admin: "admin"
};

/* authentication */
router.post('/authentication', function (req, res, next) {
    elastic
        .authentication(req.body)
        .then(function (result) {
            if (result.hits.total != 0) {
                var obj = result;
                delete obj.hits.hits[0]._source.password;
                var token = jwt.sign(payload, superSecret, {expiresIn: '24h'});

                obj.token = token;
                res.json(obj);
            } else {
                res.json(result);
            }
        });
});

/*Check if email exist */
router.post('/emailExistence', function (req, res, next) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var email = req.body.email || req.query.email || req.headers['email'];

    if (token == 'gd654dg@fuyffd5') {
        elastic
            .checkEmail(email)
            .then(function (result) {
                var number = result.hits.hits.length;
                if (number == 0) {
                    res.json({message: 'email does not exist'});
                } else {
                    res.json({message: 'exist'});
                }
            });
    } else {
        console.log("wrong token");
    }

});

/*Add a user */
router.post('/addDummyUser', function (req, res, next) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var email = req.body.email || req.query.email || req.headers['email'];

    if (token == 'gd654dg@fuyffd5') {
        elastic
            .checkEmail(email)
            .then(function (result) {
                var number = result.hits.hits.length;
                if (number == 0) {
                    elastic
                        .addUser(req.body)
                        .then(function (result) {
                            res.json(result)
                        });
                } else {
                    res.json({message: 'exist'});
                }

            });
    } else {
        console.log("nope stay hear ");
    }

});

/*Send a validation mail*/
router.post('/sendValidationMail', function (req, res, next) {

    nodemailer
        .createTestAccount()
        .then(function () {
            let transporter = nodemailer.createTransport({
                service: 'gmail',
                host: 'smtp.gmail.com',
                secure: true,
                port: 465,
                auth: {
                    user: 'apps@taurus.co.com',
                    pass: "J'm){V4z"
                }
            })

            let mailOptions = {
                from: 'Swiaa',
                to: req.body.email,
                subject: 'Swiaa Email verification',
                text: "Welcome to Archive ! Your almost ready to become a part of Swiaa's community. Si" +
                        "mply click the following button to activate your account \n http://127.0.0.1:808" +
                        "0/Activation/id=" + req.body.id
            };

            transporter
                .sendMail(mailOptions)
                .then(() => {
                    res.json(result);
                })
                .catch(err => res.json(err))
        })
        .catch(err => res.json(err))

});

/*Validate Email*/
router.post('/validateEmail', function (req, res, next) {
    elastic
        .validateEmail(req.body)
        .then(function (result) {
            res.json(result)
        });
});

module.exports = router;

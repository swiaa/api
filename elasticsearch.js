var elasticsearch = require('elasticsearch');
var elasticClient = new elasticsearch.Client({host: '127.0.0.1:9200', log: 'info'});

exports.indexExists = (index) => {
    return elasticClient
        .indices
        .exists({index: index});
}

exports.initIndex = (index) => {
    return elasticClient
        .indices
        .create({index: index});
}

exports.authentication = (user) => {
    return elasticClient.search({
        index: "users",
        type: "account",
        body: {
            query: {
                bool: {
                    should: [
                        {
                            multi_match: {
                                query: (user.email + " " + user.password),
                                type: "cross_fields",
                                fields: [
                                    "email", "password"
                                ],
                                minimum_should_match: "100%"
                            }
                        }
                    ]
                }
            }
        }
    })

}

exports.checkEmail = (email) => {
    return elasticClient.search({
        index: "users",
        type: "account",
        body: {
            query: {
                bool: {
                    should: [
                        {
                            multi_match: {
                                query: email,
                                type: "cross_fields",
                                fields: ["email"],
                                minimum_should_match: "100%"
                            }
                        }
                    ]
                }
            }
        }
    })
}

exports.addUser = (document) => {
    let created_at = new Date();

    // let hash = bcrypt.hashSync(document.password, 10);
    return elasticClient.index({
        index: "users",
        type: "account",
        body: {
            name: document.name,
            email: document.email,
            password: document.password,
            gender: document.gender,
            mailvalidation: document.mailvalidation,             
            country: document.country,
            phone: document.phone,
            profilepic: document.profilepic,
            created_at
        }
    });
}

exports.validateEmail = (object) => {
    return elasticClient.bulk({
        body: [
            {
                update: {
                    _index: 'users',
                    _type: 'account',
                    _id: object.id
                }
            },
            {
                doc: {
                    mailvalidation: object.mailvalidation
                }
            }
        ]
    });
}




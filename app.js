var express = require('express');
var cors = require('cors') ; 
var bodyParser = require('body-parser');
var app = express();  
var indexRouter = require('./routes/index');    

var users = require('./routes/users');  
var elastic = require('./elasticsearch'); 

app.use(cors()) ;
app.use(bodyParser.json({extended: true,limit: '6000mb'}));
app.use(bodyParser.urlencoded({extended: true,limit: '6000mb'}));

app.use('/users', users);

elastic.indexExists('users').then(function (exists) {  
  if (exists) {
    // return elastic.deleteIndex('users');
  }
}).then(elastic.initIndex('users'));

module.exports = app;
